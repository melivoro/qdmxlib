QT -= gui
CONFIG += c++17 plugin
DEFINES += FTD2XX_STATIC
TEMPLATE = lib
LIBS += -L$$PWD/ -lftd2xx
QMAKE_LFLAGS += /NODEFAULTLIB:LIBCMT

SOURCES += \
    QDmxUsbPlugin.cpp \
    QDmxUsbDevice.cpp \
    QDmxUsbInterface.cpp \
    QDmxEnttecOpen.cpp \
    QDmxFTD2XXInterface.cpp

HEADERS += \
    QDmxUsbPlugin.h \
    QDmxUsbDevice.h \
    QDmxUsbInterface.h \
    QDmxEnttecOpen.h \
    QDmxFTD2XXInterface.h \
    QDmxIO.h \
    ftd2xx.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
